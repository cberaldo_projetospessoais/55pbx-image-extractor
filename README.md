# Image Extractor API

## Requirements

`/extract?{url}`

* ~~Create NestJS project~~
* ~~Implement socket use~~
* Receive URL
* Extract images
* Store file in some folder
* Consider memory consume and processing
* Store page and file link in database ([MongoDB](https://www.mongodb.com/))
